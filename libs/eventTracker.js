module.exports = function(s,config,app){
    if(config.amplitudeApiToken){
        var exec = require('child_process').exec
        var trackerName = config.amplitudeName || "shinobi.video"
        console.log('Amplitude Tracking is Enabled : ' + trackerName)
        var amplitudeEndpoint = 'https://api.amplitude.com/httpapi?api_key='+config.amplitudeApiToken+'&event='
        var trackEvent = function(type,eventData,callback){
            if(!type)type = 'Untitled Event'
            if(!eventData)eventData = null
            if(typeof eventData === 'function'){
                var callback = eventData
                var eventData = null
            }
            var sendData = [
                {
                  "user_id": eventData.IP,
                  "event_type": type,
                  "event_properties": eventData
                }
            ]
            var eventDataForUrl = encodeURIComponent(JSON.stringify(sendData))
            exec("curl -m 3 -v '" + amplitudeEndpoint + eventDataForUrl + "'",function(err,data){
                if(err)console.log(err,data)
                if(callback)callback(err,data)
            })
        }
        s.trackEvent = trackEvent
        app.use(function (req, res, next) {
            next()
            trackEvent('Request',{
                "Site": "shinobi.video",
                "Page": req.originalUrl,
                "Referrer": req.headers.referrer || req.headers.referer,
                "Protocol": req.protocol,
                "IP": req.headers['cf-connecting-ip']||req.headers["CF-Connecting-IP"]||req.headers["'x-forwarded-for"]||req.connection.remoteAddress
            })
        })
    }else{
        s.trackEvent = (data) => {
            console.log(data)
        }
        console.log('Amplitude Tracking is Disabled')
    }
}
