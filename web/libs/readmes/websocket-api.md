> Socket.IO must already be included on the web page or server app.
> This tutorial assumes you have jQuery loaded already.

### Connect WebSocket

```
const shinobiWebsocket = io(location.origin,{transports: ['websocket']})
```

### Authenticate WebSocket

> xxx.xxx.xxx.xxx is your Shinobi server IP and Port.

```
$.post('http://xxx.xxx.xxx.xxx/?json=true',{
    machineID: "ANYTHINGHERE",
    mail: "ccio@m03.ca",
    pass: "123",
    function: "dash"
},(d) => {
    const user = d.$user
    const sessionKey = user.auth_token
    const groupKey = user.ke
    const userId = user.uid
    const emitToServer = (data) => {
        if(!data.ke)data.ke = groupKey;
        if(!data.uid)data.uid = userId;
        return shinobiWebsocket.emit('f',data)
    }
    // send init request
    emitToServer({
        f: 'init',
        ke: groupKey,
        auth: sessionKey,
        uid: userId
    })

})
```

### Capture Events

```
shinobiWebsocket.on('f',(data) => {
    switch(data.f){
        case'detector_trigger':
            console.log('detector_trigger',data)
        break;
        case'log':
            console.log('log',data)
        break;
    }
})
```

### Begin Receiving Live Information about a Monitor

> this would be executed after logging in with the $.post request. See `emitToServer` reference in "Authenticate WebSocket".

```
emitToServer({
    f: 'monitor',
    ff: 'watch_on',
    id: monitorId
})
```
